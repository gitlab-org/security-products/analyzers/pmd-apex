require 'English'

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def project_dir
    '/app'
  end

  def image_name
    ENV.fetch('TMP_IMAGE', 'pmd-apex:latest')
  end

  context 'with no project' do
    before(:context) do
      @output = `docker run -t --rm --user=gitlab -w #{project_dir} #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it 'shows there is no match' do
      expect(@output).to match(/no match in #{project_dir}/i)
    end

    describe 'exit code' do
      specify { expect(@exit_code).to be 0 }
    end
  end

  context 'with test project' do
    def parse_expected_report(expectation_name, report_name = 'gl-sast-report.json')
      path = File.join(expectations_dir, expectation_name, report_name)
      JSON.parse(File.read(path))
    end

    let(:secure_log_level) { 'debug' }

    let(:global_vars) do
      {
        # keep the json report indented so we can easily view the contents if tests fail
        'ANALYZER_INDENT_REPORT': 'true',
        'SECURE_LOG_LEVEL': secure_log_level
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command:,
        script:,
        offline:,
        user: 'gitlab',
        variables: global_vars.merge(variables),
        report_filename: 'gl-sast-report.json')
    end

    let(:report) { scan.report }

    context 'simple apex project' do
      let(:project) { 'apex-salesforce/default' }
      let(:variables) do
        { 'GITLAB_FEATURES': 'vulnerability_finding_signatures' }
      end

      it_behaves_like 'successful scan'

      describe 'created report' do
        it_behaves_like 'non-empty report'

        it_behaves_like 'recorded report' do
          let(:recorded_report) { parse_expected_report(project) }
        end

        it_behaves_like 'valid report'
      end

      context 'when testing log output' do
        context 'when SECURE_LOG_LEVEL is set to debug' do
          it 'outputs debug log messages' do
            expect(scan.combined_output).to match(/\[DEBU\] \[PMD\.Apex\] \[.*\] \[.*\] ▶ Applying report overrides/)
          end

          it 'streams the output of the pmd command at the info level' do
            expect(scan.combined_output).to match(/\[INFO\] \[PMD\.Apex\] \[.*\] ▶ .* net\.sourceforge\.pmd\.PMD runPmd/)
          end
        end

        context 'when SECURE_LOG_LEVEL is not set' do
          let(:secure_log_level) { nil }

          it 'does not output debug log messages' do
            expect(scan.combined_output).not_to match(/\[DEBU\] \[PMD\.Apex\] \[.*\] \[.*\] ▶ Applying report overrides/)
          end

          it 'streams the output of the pmd command at the info level' do
            expect(scan.combined_output).to match(/\[INFO\] \[PMD\.Apex\] \[.*\] ▶ .* net\.sourceforge\.pmd\.PMD runPmd/)
          end
        end
      end
    end

    context 'simple apex project with disabled rules' do
      let(:project) { 'apex-salesforce/with-disabled-rules' }
      let(:variables) do
        { 'GITLAB_FEATURES': 'vulnerability_finding_signatures sast_custom_rulesets' }
      end

      it_behaves_like 'successful scan'

      describe 'created report' do
        it_behaves_like 'non-empty report'

        it_behaves_like 'recorded report' do
          let(:recorded_report) { parse_expected_report(project) }
        end

        it_behaves_like 'valid report'
      end
    end
  end
end
